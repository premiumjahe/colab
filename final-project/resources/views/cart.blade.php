@extends('index')
@section('title')
halaman cart
@endsection

@section('content')
<section class="vh-100" style="background-color: #536463;">
    <div class="container h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col">
          <p><span class="h2">Shopping Cart </span><span class="h4">(1 item in your cart)</span></p>
  
          <div class="card mb-4">
            <table class="table">
                <thead>
                  <tr class="text-center">
                    <th scope="col">No</th>
                    <th scope="col">game</th>
                    <th scope="col">jumlah</th>
                    <th scope="col">harga</th>
                    <th scope="col">aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="text-center">
                    <th scope="row">1</th>
                    <td>ml</td>
                    <td>2</td>
                    <td>$23</td>
                    <td>
                        <form action="#" method="post">
                            @csrf
                            @method('delete')
                            <a href="#" class="btn btn-sm btn-warning mx-2"><i class="bi bi-pencil-fill"></i></a>
                            <button type="submit" class="btn btn-sm btn-danger mx-2"><i class="bi bi-trash3-fill"></i></button>
                          </form>              
                    </td>
                  </tr>
                </tbody>
              </table>
          </div>
  
          <div class="card mb-5">
            <div class="card-body p-4">
  
              <div class="float-end">
                <p class="mb-0 me-5 d-flex align-items-center">
                  <span class="small text-muted me-2">Order total:</span> <span
                    class="lead fw-normal">$799</span>
                </p>
              </div>
  
            </div>
          </div>
  
          <div class="d-flex justify-content-end">
            <a class="btn btn-light btn-lg me-2" href="/">Continue shopping</a>
            <button type="button" class="btn btn-primary btn-lg">Checkout</button>
          </div>
  
        </div>
      </div>
    </div>
  </section>
@endsection
