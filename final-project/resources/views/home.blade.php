@extends('index')
@section('title')
halaman home
@endsection

@section('header')
<header class="bg-dark py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="text-center text-white">
            <h1 class="display-4 fw-bolder">home</h1>
            <p class="lead fw-normal text-white-50 mb-0">halaman kategori</p>
        </div>
    </div>
</header>
@endsection

@section('content')
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <div class="col mb-5">
                <div class="card h-100">
                    <!-- Product image-->
                    <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." />
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="text-center">
                            <!-- Product name-->
                            <h5 class="fw-bolder">Fancy Product</h5>
                            <!-- Product price-->
                            <p>start from : $40.00 - $80.00</p>
                        </div>
                    </div>
                    <!-- Product actions-->
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center">
                            <form action="" method="post">
                                @csrf
                                @method('delete')
                                <a class="btn btn-outline-primary mt-auto w-100 m-1" href="/store"><i class="bi bi-rocket-takeoff-fill"></i></a>
                                <a class="btn btn-outline-warning mt-auto w-50  m-1" href="#"><i class="bi bi-pencil-fill"></i></a>
                                <a class="btn btn-outline-danger mt-auto w-25 m-1" href="#"><i class="bi bi-trash3-fill"></i></a>  
                            </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection